Coderwall Block
==============================

This module provides a block with Coderwall Badges.

Installation.
=============

1. Unzip the files to the "sites/all/modules" directory and enable the module.

2. Go to the admin/structure/block page to enable and configure the Coderwall Block.

Credit
======

The module utilises the Coderwall API and does not integrate with userprofiles (like https://drupal.org/project/coderwall) or https://drupal.org/project/easy_social
https://coderwall.com/api#blogbadge

