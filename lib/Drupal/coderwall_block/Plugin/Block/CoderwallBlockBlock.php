<?php
/**
 * @file
 * Contains \Drupal\user\Plugin\block\block\CoderwallBlockBlock.
 */

namespace Drupal\coderwall_block\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\block\Annotation\Block;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a "Coderwall Block" block.
 *
 * @Block(
 *   id = "coderwall_block",
 *   admin_label = @Translation("Coderwall Block"),
 *   module = "coderwall_block"
 * )
 */
class CoderwallBlockBlock extends BlockBase {
  /**
   * Overrides \Drupal\block\BlockBase::defaultConfiguration().
   */
  public function defaultConfiguration() {
    return array(
      'properties' => array(
        'administrative' => TRUE
      ),
      'orientation' => 'horizontal',
      'username' => 'none'
    );
  }
  /**
   * Overrides \Drupal\block\BlockBase::settings().
   */
  public function defaultConfiguration() {
    return array(
      'properties' => array(
        'administrative' => TRUE
      ),
      'orientation' => 'horizontal',
      'username' => 'none'
    );
  }
  /**
   * Overrides \Drupal\block\BlockBase::blockForm().
   */
  public function blockForm($form, &$form_state) {

    $form['coderwall_block_orientation'] = array(
      '#type' => 'select',
      '#title' => t('Block orientation'),
      '#options' => array('horizontal', 'vertical'),
      '#default_value' => $this->configuration['orientation']
    );
    $form['coderwall_block_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Block username'),
      '#default_value' => $this->configuration['username'],
      '#description' => t('your username on coderwall.')
    );
    return $form;
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockSubmit().
   */
  public function blockSubmit($form, &$form_state) {
    $this->configuration['orientation'] = $form_state['values']['coderwall_block_orientation'];
    $this->configuration['username'] = $form_state['values']['coderwall_block_username'];
  }

  /**
   * Implements \Drupal\block\BlockBase::build().
   */
  public function build() {
    $build = array();
    $switch = 'coderwall';
    $uname = $this->configuration['username'];
    $orient = $this->configuration['orientation'];
    
    $build['#markup'] = "<section class=" . $switch . " data-coderwall-username=" . $uname . " data-coderwall-orientation=" . $orient . "></section>";

    $build['#attached']['css'] = array(
      'http://coderwall.com/stylesheets/jquery.coderwall.css' => array(
        'type' => 'external',
      ),
    );
    $build['#attached']['js'] = array(
      'http://coderwall.com/javascripts/jquery.coderwall.js' => array(
        'type' => 'external',
      ),
    );

    return $build;
  }
}
?>
